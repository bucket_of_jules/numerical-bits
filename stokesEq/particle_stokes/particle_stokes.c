#include <stdio.h>
#include <petsc.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscdmplex.h>
#include <petscdmswarm.h>
#include <petscviewerhdf5.h>
#include <mpi.h>

typedef struct {
  char filename[2048]; /* The mesh file */
  PetscInt elements[3];
  PetscInt dim;
  PetscBool reload;
  DM        swarm;
  PetscBool use_simplex;
} AppCtx;


static PetscErrorCode zero_vector(PetscInt dim, PetscReal time, const PetscReal coords[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  PetscInt d;
  for (d = 0; d < dim; ++d) u[d] = 0.0;
  return 0;
}

static PetscErrorCode coord_vector(PetscInt dim, PetscReal time, const PetscReal coords[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  PetscInt d;
  for (d = 0; d < dim; ++d) u[d] = coords[d];
  return 0;
}

static PetscErrorCode one_scalar(PetscInt dim, PetscReal time, const PetscReal coords[], 
                                  PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = 1.0;
  return 0;
}

static PetscErrorCode zero_scalar(PetscInt dim, PetscReal time, const PetscReal coords[], 
                                  PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = 0.0;
  return 0;
}

static PetscErrorCode density_scalar(PetscInt dim, PetscReal time, const PetscReal coords[], 
                                 PetscInt Nf, PetscScalar *u, void *ctx)
{
  PetscScalar circle;

  // circle geometry. If inside set gravity force, otherwise 0 force
  if(dim==3) circle = pow(coords[0],2)+pow(coords[1]-0,2)+pow(coords[2]-7,2);
  else       circle = pow(coords[0],2)+pow(coords[1]-0,2);
  if (circle < pow(3,2) ) {
    u[0] = 1.5; 
  } else {
    u[0] = 1;
  }
  return 0;
}


static void f0_u(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  PetscInt c;

  f0[dim-1] = a[0];
  for(c=dim-2; c>=0; c--) {
    f0[c] = 0;
  }

}

/* [P] The pointwise functions below describe all the problem physics */

/* gradU[comp*dim+d] = {u_x, u_y, v_x, v_y} or {u_x, u_y, u_z, v_x, v_y, v_z, w_x, w_y, w_z}
   u[Ncomp]          = {p} */
static void f1_u(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f1[])
{
  const PetscInt Ncomp = dim;
  PetscInt       comp, d;

  for (comp = 0; comp < Ncomp; ++comp) {
    for (d = 0; d < dim; ++d) {
      /* f1[comp*dim+d] = 0.5*(gradU[comp*dim+d] + gradU[d*dim+comp]); */
      f1[comp*dim+d] = u_x[comp*dim+d];
    }
    f1[comp*dim+comp] -= u[Ncomp];
  }
}

/* gradU[comp*dim+d] = {u_x, u_y, v_x, v_y} or {u_x, u_y, u_z, v_x, v_y, v_z, w_x, w_y, w_z} */
static void f0_p(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  PetscInt d;
  f0[0] = 0.0;
  for (d = 0, f0[0] = 0.0; d < dim; ++d) f0[0] += u_x[d*dim+d];
}
void f1_p(PetscInt dim, PetscInt Nf, PetscInt NfAux,
          const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
          const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
          PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f1[])
{
  PetscInt d;
  for (d = 0; d < dim; ++d) f1[d] = 0.0;
}

/* < q, \nabla\cdot u >
   NcompI = 1, NcompJ = dim */
static void j1_pu(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g1[])
{
  PetscInt d;
  for (d = 0; d < dim; ++d) g1[d*dim+d] = 1.0; /* \frac{\partial\phi^{u_d}}{\partial x_d} */
}

/* -< \nabla\cdot v, p >
    NcompI = dim, NcompJ = 1 */
static void j2_up(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g2[])
{
  PetscInt d;
  for (d = 0; d < dim; ++d) g2[d*dim+d] = -1.0; /* \frac{\partial\psi^{u_d}}{\partial x_d} */
}

/* < \nabla v, \nabla u + {\nabla u}^T >
   This just gives \nabla u, give the perdiagonal for the transpose */
static void j3_uu(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g3[])
{
  const PetscInt Nc = dim;
  PetscInt       c, d;

  for (c = 0; c < Nc; ++c) {
    for (d = 0; d < dim; ++d) {
      g3[((c*Nc+c)*dim+d)*dim+d] = 1.0;
    }
  }
}

static void stokes_momentum_J(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                                     const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                                     const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                                     PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g3[])
{
  const PetscReal nu  = 1.;
  PetscInt        cI, d;

  for (cI = 0; cI < dim; ++cI) {
    for (d = 0; d < dim; ++d) {
      g3[((cI*dim+cI)*dim+d)*dim+d] += nu; /*g3[cI, cI, d, d]*/
      g3[((cI*dim+d)*dim+d)*dim+cI] += nu; /*g3[cI, d, d, cI]*/
    }
  }
}

/* 1/nu < q, I q >, Jp_{pp} */
static void massMatrix_invEta(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                                 PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g0[])
{
  const PetscReal nu = 1.;//PetscExpReal(2.0*PetscRealPart(a[2])*x[0]);
  g0[0] = 1.0/nu;
}

static PetscErrorCode SetupProblem(DM dm, PetscDS prob, AppCtx *user)
{
  const PetscInt          ids[2]   = {0,1}; /* scalar */
  PetscErrorCode          ierr;

  PetscFunctionBeginUser;
  ierr = PetscDSSetResidual(prob, 0, f0_u, f1_u);CHKERRQ(ierr);
  ierr = PetscDSSetResidual(prob, 1, f0_p, f1_p);CHKERRQ(ierr);
  
  ierr = PetscDSSetJacobian(prob, 0, 0, NULL,  NULL,  NULL, j3_uu);CHKERRQ(ierr);
  ierr = PetscDSSetJacobian(prob, 0, 1, NULL,  NULL, j2_up,  NULL);CHKERRQ(ierr);
  ierr = PetscDSSetJacobian(prob, 1, 0, NULL, j1_pu,  NULL,  NULL);CHKERRQ(ierr);

  /*
  ierr = PetscDSSetJacobianPreconditioner(prob, 0, 0, NULL, NULL, NULL, stokes_momentum_J);CHKERRQ(ierr);
  ierr = PetscDSSetJacobianPreconditioner(prob, 0, 1, NULL, NULL, j2_up, NULL);CHKERRQ(ierr);
  ierr = PetscDSSetJacobianPreconditioner(prob, 1, 0, NULL, j1_pu, NULL, NULL);CHKERRQ(ierr);
  ierr = PetscDSSetJacobianPreconditioner(prob, 1, 1, massMatrix_invEta, NULL, NULL, NULL);CHKERRQ(ierr);
  */
  //ierr = PetscDSAddBoundary(prob, DM_BC_ESSENTIAL, 
  //  "wall", "marker", 0, 0, NULL, (void (*)(void)) zero_vector, 1, ids, user);CHKERRQ(ierr);
  ierr = PetscDSAddBoundary(prob, DM_BC_ESSENTIAL, 
    "wall", "marker", 0, 0, NULL, (void (*)(void)) zero_vector, 1, ids+1, user);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

static PetscErrorCode SetupAux(DM dm, DM dmAux, AppCtx *user)
{
  PetscErrorCode (*matFuncs[1])(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nc, PetscScalar u[], void *ctx) = {density_scalar}; //,one_scalar};
  Vec            auxVec,gVec;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  ierr = DMCreateLocalVector(dmAux, &auxVec);CHKERRQ(ierr);
  // create global to output only
  ierr = DMCreateGlobalVector(dmAux, &gVec);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) gVec, "auxiliary");CHKERRQ(ierr);

  // reload or create
  if ( user->reload==PETSC_TRUE ) {
    PetscViewer viewer;
    PetscViewerHDF5Open(PETSC_COMM_WORLD, "aux.h5", FILE_MODE_READ, &viewer);
    PetscViewerHDF5PushGroup(viewer, "/fields");
    VecLoad(gVec, viewer);
    ierr = PetscViewerHDF5PopGroup(viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    DMGlobalToLocalBegin(dmAux, gVec, INSERT_VALUES, auxVec);
    DMGlobalToLocalEnd(dmAux, gVec, INSERT_VALUES, auxVec);
   } else {
    ierr = DMProjectFunctionLocal(dmAux, 0.0, matFuncs, NULL, INSERT_ALL_VALUES, auxVec);CHKERRQ(ierr);
    ierr = DMLocalToGlobalBegin( dmAux, auxVec, INSERT_VALUES, gVec );
    ierr = DMLocalToGlobalEnd( dmAux, auxVec, INSERT_VALUES, gVec );
    ierr = DMViewFromOptions( dmAux, NULL, "-dmAux_view");CHKERRQ(ierr);
    ierr = VecViewFromOptions(gVec, NULL, "-aux_vec_view");CHKERRQ(ierr);
   }

  ierr = VecDestroy(&gVec);CHKERRQ(ierr);
  // increases reference count
  ierr = PetscObjectCompose((PetscObject) dm, "A", (PetscObject) auxVec);CHKERRQ(ierr);
  ierr = VecDestroy(&auxVec);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode initialise_swarm(DM* _swarm) 
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscReal      *coords,*eta,x,y,z;
  PetscInt       p_i,bs,lsize;
  DM             swarm = *_swarm;

  ierr = DMSwarmGetLocalSize(swarm,&lsize);CHKERRQ(ierr);
  ierr = DMSwarmGetField(swarm, DMSwarmPICField_coor, &bs, NULL, (void**)&coords );CHKERRQ(ierr);
  ierr = DMSwarmGetField(swarm, "pdensity", NULL, NULL, (void**)&eta );CHKERRQ(ierr);

  for(p_i=0;p_i<lsize;p_i++) {
    x = coords[p_i*(bs)+0];
    y = coords[p_i*(bs)+1];
    //if (dim == 3) z = coords[p_i*(bs)+2];
    eta[p_i] = 0.5;
    if( (x*x + y*y) < (2.5*2.5) ) {
      eta[p_i] = 1.0;
    }
  }
  ierr = DMSwarmRestoreField(swarm, DMSwarmPICField_coor, &bs, NULL, (void**)&coords );CHKERRQ(ierr);
  ierr = DMSwarmRestoreField(swarm, "pdensity", NULL, NULL, (void**)&eta );CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

static PetscErrorCode SetupSwarm(DM* celldm, DM* swarm) {
  PetscErrorCode ierr;
  PetscInt       dim;

  PetscFunctionBeginUser;

  ierr = DMGetDimension(*celldm, &dim);CHKERRQ(ierr);
  ierr = DMCreate(PETSC_COMM_WORLD,swarm);CHKERRQ(ierr);
  ierr = DMSetType(*swarm,DMSWARM);CHKERRQ(ierr);
  ierr = DMSetDimension(*swarm,dim);CHKERRQ(ierr);

  ierr = DMSwarmSetType(*swarm,DMSWARM_PIC);CHKERRQ(ierr);
  ierr = DMSwarmSetCellDM(*swarm,*celldm);CHKERRQ(ierr);

  /* Register two scalar fields within the DMSwarm */
  ierr = DMSwarmRegisterPetscDatatypeField(*swarm,"pdensity",1,PETSC_DOUBLE);CHKERRQ(ierr);
  ierr = DMSwarmFinalizeFieldRegister(*swarm);CHKERRQ(ierr);

  /* Set initial local sizes of the DMSwarm with a buffer length of zero */
  ierr = DMSwarmSetLocalSizes(*swarm,4,0);CHKERRQ(ierr);

  /* Insert swarm coordinates cell-wise */
  ierr = DMSwarmInsertPointsUsingCellDM(*swarm,DMSWARMPIC_LAYOUT_GAUSS,1);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode SwarmProjectToCells(DM* celldm, DM* swarm, int nfields, const char *fieldnames[]) {
  
  Vec lvec, pVec, *ptrV;
  DM dmAux;

  PetscErrorCode ierr;
  
  // get previously used projection vector and auxilliary dm/vector & re-project
#if 0
  PetscObjectQuery( (PetscObject)*celldm, "projVec", (PetscObject*)&pVec);
#endif
  PetscObjectQuery( (PetscObject)*celldm, "dmAux",   (PetscObject*)&dmAux);
  PetscObjectQuery( (PetscObject)*celldm, "A",       (PetscObject*)&lvec);
  ptrV = &pVec;

  ierr = DMSwarmProjectFields(*swarm,nfields,fieldnames,&ptrV,PETSC_FALSE);
  ierr = DMGlobalToLocalBegin(dmAux, ptrV[0], INSERT_VALUES, lvec);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmAux, ptrV[0], INSERT_VALUES, lvec);CHKERRQ(ierr);

  PetscObjectSetName((PetscObject) ptrV[0], "proj2");
  PetscObjectViewFromOptions((PetscObject)ptrV[0], NULL, "-pro2_view");

  return 0;
}


PetscErrorCode SwarmAdvectRK1(DM* celldm, Vec* gridvel, DM* swarm, PetscReal dt) {
  /**
   * Advects a DMSwarm, dt in time, using a velocity vector from another DM
   **/
  PetscFunctionBegin;
  DMInterpolationInfo ipInfo;
  Vec                 pvel;
  PetscReal           *coords;
  PetscInt            lsize, p,d, dim, blockSize;
  const PetscScalar   *pv;
  PetscErrorCode      ierr;

  /* get a vector fields on the swarm */
  ierr = DMSwarmVectorDefineField(*swarm, DMSwarmPICField_coor);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(*swarm, &pvel);CHKERRQ(ierr);
  ierr = DMSwarmGetLocalSize(*swarm, &lsize);CHKERRQ(ierr);

  // create interpolation
  ierr = DMInterpolationCreate(PetscObjectComm((PetscObject)*celldm), &ipInfo);
  ierr = DMGetDimension(*celldm, &dim);
  ierr = DMInterpolationSetDim(ipInfo, dim);
  ierr = DMInterpolationSetDof(ipInfo, dim);

  // add points
  ierr = DMSwarmGetField(*swarm, DMSwarmPICField_coor, NULL, NULL, (void **) &coords);CHKERRQ(ierr);
  ierr = DMInterpolationAddPoints(ipInfo, lsize, coords);CHKERRQ(ierr);
  ierr = DMSwarmRestoreField(*swarm, DMSwarmPICField_coor, NULL, NULL, (void **) &coords);CHKERRQ(ierr);

  ierr = DMInterpolationSetUp(ipInfo, *celldm, PETSC_FALSE);CHKERRQ(ierr);
  ierr = DMInterpolationEvaluate(ipInfo, *celldm, *gridvel, pvel);CHKERRQ(ierr);
  ierr = DMInterpolationDestroy(&ipInfo);CHKERRQ(ierr);

  // update locations of points
  ierr = DMSwarmGetField(*swarm, DMSwarmPICField_coor, &blockSize, NULL, (void**)&coords);CHKERRQ(ierr);
  ierr = VecGetArrayRead(pvel, &pv);CHKERRQ(ierr);
  for(p=0; p<lsize; p++) {
    for(d=0; d<dim; d++) coords[p*dim+d] += pv[p*dim+d] * dt;
  }
  ierr = VecRestoreArray(pvel, (PetscScalar**)&pv);CHKERRQ(ierr);
  ierr = DMSwarmRestoreField(*swarm, DMSwarmPICField_coor, NULL, NULL, (void**)&coords);CHKERRQ(ierr);
  ierr = DMSwarmMigrate(*swarm, PETSC_TRUE);CHKERRQ(ierr);
  ierr = VecDestroy(&pvel);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


static PetscErrorCode SetupDiscretizationWithParticles(DM dm, AppCtx *user, DM* swarm)
{
  DM              cdm = dm;
  PetscFE         fe[2];
  PetscQuadrature q;
  PetscDS         prob;
  PetscSpace      space;
  PetscInt        dim;
  PetscErrorCode  ierr;
  
  PetscFunctionBeginUser;

  ierr = DMGetDimension(dm, &dim);CHKERRQ(ierr);
  /* Create finite element */
  ierr = PetscFECreateDefault(PetscObjectComm((PetscObject)dm), dim, dim, user->use_simplex, "u_", PETSC_DEFAULT, &fe[0]);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe[0], "velocity");CHKERRQ(ierr);
  ierr = PetscFEGetQuadrature(fe[0], &q);CHKERRQ(ierr);
  
  ierr = PetscFECreateDefault(PetscObjectComm((PetscObject)dm), dim, 1, user->use_simplex, "p_", PETSC_DEFAULT, &fe[1]);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe[1], "pressure");CHKERRQ(ierr);
  ierr = PetscFESetQuadrature(fe[1], q);CHKERRQ(ierr);

  /* Set discretization and boundary conditions for each mesh */
  ierr = DMGetDS(dm, &prob);CHKERRQ(ierr);
  ierr = PetscDSSetDiscretization(prob, 0, (PetscObject) fe[0]);CHKERRQ(ierr);
  ierr = PetscDSSetDiscretization(prob, 1, (PetscObject) fe[1]);CHKERRQ(ierr);
  ierr = SetupProblem(dm, prob, user);CHKERRQ(ierr);

  DM da2;
  PetscDS probAux;
  PetscFE projFe;
  Vec projV;

  // build 
  ierr = DMClone(dm, &da2);CHKERRQ(ierr);
  ierr = DMSetFromOptions(da2);CHKERRQ(ierr);
  DMSetUp(da2);
  PetscObjectViewFromOptions((PetscObject)da2, NULL, "-dm2_view");

  ierr = SetupSwarm(&da2, swarm);CHKERRQ(ierr);
  ierr = initialise_swarm(swarm);

  ierr = PetscFECreateDefault(PetscObjectComm((PetscObject)dm), dim, 1, user->use_simplex, "smooth", PETSC_DECIDE, &projFe);
  PetscObjectSetName((PetscObject) projFe, "smooth density");
  DMGetDS(da2, &probAux);
  PetscDSSetDiscretization(probAux, 0, (PetscObject) projFe);
  DMSetDS(da2, probAux);
  ierr = PetscFEDestroy(&projFe);CHKERRQ(ierr); 

  Vec localvec, *pvecs;
  const char filename[] = "swarm.xmf";
  PetscInt   nfields = 1;
  const char *flist[] = {"pdensity"}; 

  DMCreateLocalVector(da2, &localvec);

  DMSwarmViewFieldsXDMF(*swarm,filename,nfields,flist);
  ierr = DMSwarmProjectFields(*swarm,nfields,flist,&pvecs,PETSC_FALSE);
  ierr = DMGlobalToLocalBegin(da2, pvecs[0], INSERT_VALUES, localvec);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da2, pvecs[0], INSERT_VALUES, localvec);CHKERRQ(ierr);
  PetscObjectSetName((PetscObject) pvecs[0], "proj");
  PetscObjectViewFromOptions((PetscObject)pvecs[0], NULL, "-pro_view");
  ierr = PetscObjectCompose((PetscObject) dm, "dmAux", (PetscObject) da2);CHKERRQ(ierr);
  ierr = PetscObjectCompose((PetscObject) dm, "A", (PetscObject) localvec);CHKERRQ(ierr);
  VecDestroy(&localvec);
  VecDestroy(&pvecs[0]);
  //PetscFree(&pvecs);
  DMDestroy(&da2);

  /*
    Vec *list = &projV;
    const char filename[] = "swarm.xmf";
    PetscInt   nfields = 1;
    const char *flist[] = {"pdensity"}; 
    DMSwarmViewFieldsXDMF(*swarm,filename,nfields,flist);
    ierr = DMSwarmProjectFields(*swarm,nfields,flist,&list,PETSC_TRUE);
    PetscObjectSetName((PetscObject) projV, "proj");
    PetscObjectViewFromOptions((PetscObject)projV, NULL, "-pro_view");
    //
  // associate the auxiliary dm and vector to the original problem dm
  ierr = PetscObjectCompose((PetscObject) dm, "dmAux", (PetscObject) da2);CHKERRQ(ierr);
  ierr = PetscObjectCompose((PetscObject) dm, "A", (PetscObject) projV);CHKERRQ(ierr);
 
  // clean up
  DMDestroy(&da2);
  VecDestroy(&projV);
  */
  
  {
    PetscObject  pressure;
    MatNullSpace nullSpacePres;

    ierr = DMGetField(dm, 1, &pressure);CHKERRQ(ierr);
    ierr = MatNullSpaceCreate(PetscObjectComm(pressure), PETSC_TRUE, 0, NULL, &nullSpacePres);CHKERRQ(ierr);
    ierr = PetscObjectCompose(pressure, "nullspace", (PetscObject) nullSpacePres);CHKERRQ(ierr);
    ierr = MatNullSpaceDestroy(&nullSpacePres);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode CreatePressureNullSpace(DM dm, PetscInt dummy, MatNullSpace *nullspace)
{
  Vec              vec;
  PetscErrorCode (*funcs[2])(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void* ctx) = {zero_vector, one_scalar};
  PetscErrorCode   ierr;

  PetscFunctionBeginUser;
  ierr = DMCreateGlobalVector(dm, &vec);CHKERRQ(ierr);
  ierr = DMProjectFunction(dm, 0.0, funcs, NULL, INSERT_ALL_VALUES, vec);CHKERRQ(ierr);
  ierr = VecNormalize(vec, NULL);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) vec, "Pressure Null Space");CHKERRQ(ierr);
  ierr = VecViewFromOptions(vec, NULL, "-pressure_nullspace_view");CHKERRQ(ierr);
  ierr = MatNullSpaceCreate(PetscObjectComm((PetscObject)dm), PETSC_FALSE, 1, &vec, nullspace);CHKERRQ(ierr);
  ierr = VecDestroy(&vec);CHKERRQ(ierr);
  /* New style for field null spaces */
  {
    PetscObject  pressure;
    MatNullSpace nullspacePres;

    ierr = DMGetField(dm, 1, &pressure);CHKERRQ(ierr);
    ierr = MatNullSpaceCreate(PetscObjectComm(pressure), PETSC_TRUE, 0, NULL, &nullspacePres);CHKERRQ(ierr);
    ierr = PetscObjectCompose(pressure, "nullspace", (PetscObject) nullspacePres);CHKERRQ(ierr);
    ierr = MatNullSpaceDestroy(&nullspacePres);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* OLD
static PetscErrorCode CreatePressureNullSpace(DM dm, AppCtx *user, Vec *v, MatNullSpace *nullSpace)
{
  Vec              vec;
  PetscErrorCode (*funcs[2])(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void* ctx) = {zero_vector, one_scalar};
  PetscErrorCode   ierr;

  PetscFunctionBeginUser;
  ierr = DMGetGlobalVector(dm, &vec);CHKERRQ(ierr);
  ierr = DMProjectFunction(dm, 0.0, funcs, NULL, INSERT_ALL_VALUES, vec);CHKERRQ(ierr);
  ierr = VecNormalize(vec, NULL);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) vec, "Pressure Null Space");CHKERRQ(ierr);
  ierr = VecViewFromOptions(vec, NULL, "-null_space_vec_view");CHKERRQ(ierr);
  ierr = MatNullSpaceCreate(PetscObjectComm((PetscObject) dm), PETSC_FALSE, 1, &vec, nullSpace);CHKERRQ(ierr);
  if (v) {
    ierr = DMCreateGlobalVector(dm, v);CHKERRQ(ierr);
    ierr = VecCopy(vec, *v);CHKERRQ(ierr);
  }
  ierr = DMRestoreGlobalVector(dm, &vec);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
*/

PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *user) {
  PetscInt dim;
  PetscErrorCode ierr;
  PetscBool flg;
  
  // set default elements
  user->elements[0] = 10;
  user->elements[1] = 10;
  user->elements[2] = 10; 
  user->dim=3;
  user->use_simplex=PETSC_FALSE;
  ierr = PetscOptionsBegin(comm, "", "Julian 3D Stokes test", PETSC_NULL); // must call before options
  ierr = PetscOptionsGetBool(NULL,"","-r",&(user->reload),NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,"","-simplex",&(user->use_simplex),NULL);CHKERRQ(ierr);
  ierr = PetscOptionsSetValue(NULL, "-u_petscspace_order", "1"); // default linear trail function approximation
  user->filename[0] = '\0';
  ierr = PetscOptionsString("-f", "Mesh filename to read", "stokesmodel", user->filename, user->filename, sizeof(user->filename), &flg);CHKERRQ(ierr);
  // ierr = PetscOptionsSetValue(NULL, "-dm_plex_separate_marker", ""); // must use
  ierr = PetscOptionsIntArray("-elRes", "element count (default: 5,5,5)", "n/a", user->elements, &user->dim, NULL);
  ierr = PetscOptionsEnd();
  return(0);
}

int main(int argc, char**argv) {

    PetscInt size;
    DM dm, swarm;
    Vec u;
    Vec nullVec;
    MatNullSpace nullSpace;
    AppCtx user;
    SNES snes;
    DMLabel label;
    Vec gVec, lVec, xy;
    PetscErrorCode ierr;    

    user.reload = PETSC_FALSE;

    PetscInitialize( &argc, &argv, (char*)0, NULL );
    ProcessOptions(PETSC_COMM_WORLD, &user);

    ierr = SNESCreate(PETSC_COMM_WORLD, &snes);CHKERRQ(ierr);
    const char    *filename = user.filename;
    size_t len;
    PetscStrlen(filename, &len);
    if (len>0) {
        DMPlexCreateFromFile(PETSC_COMM_WORLD, filename, PETSC_FALSE, &dm);
    } else {
        PetscReal lower[3] = {-5.,-5., 0.};
        PetscReal upper[3] = {5.,5., 10.};
        DMPlexCreateBoxMesh(PETSC_COMM_WORLD, user.dim, 
                        user.use_simplex, // use_simplices, if not then tesor_cells
                        user.elements, lower, upper, // sizes, and the min and max coords  
                        NULL, PETSC_TRUE,&dm);
    }
    /* Distribute mesh over processes */
    {
      PetscPartitioner part;
      DM               pdm = NULL;

      ierr = DMPlexGetPartitioner(dm, &part);CHKERRQ(ierr);
      ierr = PetscPartitionerSetFromOptions(part);CHKERRQ(ierr);
      ierr = DMPlexDistribute(dm, 0, NULL, &pdm);CHKERRQ(ierr);
      if (pdm) {
        ierr = DMDestroy(&dm);CHKERRQ(ierr);
        dm  = pdm;
      }
    }
    ierr = DMLocalizeCoordinates(dm);CHKERRQ(ierr); /* needed for periodic only */
    ierr = DMSetFromOptions(dm);CHKERRQ(ierr);
    
     /* Make split wall labels - closer to uw feel must run with -dm_plex_separate_marker */
    // {
    //   const char *names[4] = {"wallBottom", "wallRight", "wallTop", "wallLeft"};
    //   PetscInt    ids[4]   = {1, 2, 3, 4};
    //   DMLabel     label;
    //   IS          is;
    //   PetscInt    f;
    // 
    //   for (f = 0; f < 4; ++f) {
    //     ierr = DMGetStratumIS(dm, "marker", ids[f],  &is);CHKERRQ(ierr);
    //     if (!is) continue;
    //     ierr = DMCreateLabel(dm, names[f]);CHKERRQ(ierr);
    //     ierr = DMGetLabel(dm, names[f], &label);CHKERRQ(ierr);
    //     if (is) {
    //       ierr = DMLabelInsertIS(label, is, 1);CHKERRQ(ierr);
    //     }
    //     ierr = ISDestroy(&is);CHKERRQ(ierr);
    //   }
    // }
    SetupDiscretizationWithParticles(dm, &user, &swarm);
    /* [O] The mesh is output to HDF5 using options */
    ierr = DMViewFromOptions(dm, NULL, "-dm_view");CHKERRQ(ierr);
    ierr = DMSetApplicationContext(dm, &user);CHKERRQ(ierr);
    ierr = SNESSetDM(snes, dm); CHKERRQ(ierr);
    
    /* Calculates the index of the 'default' section, should improve performance */
    ierr = DMPlexCreateClosureIndex(dm, NULL);CHKERRQ(ierr);
    /* Sets the fem routines for boundary, residual and Jacobian point wise operations */
    ierr = DMPlexSetSNESLocalFEM(dm, &user, &user, &user);CHKERRQ(ierr);
    // OLD ierr = CreatePressureNullSpace(dm, &user, &nullVec, &nullSpace);CHKERRQ(ierr);
    ierr = DMSetNullSpaceConstructor(dm, 2, CreatePressureNullSpace);CHKERRQ(ierr);
    /* Get global vector */
    ierr = DMCreateGlobalVector(dm, &u);CHKERRQ(ierr);
    /* Update SNES */
    ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
    ierr = SNESSetUp(snes);
    ierr = SNESView(snes, PETSC_VIEWER_STDOUT_WORLD);
    
    /* Solve and output*/
    PetscErrorCode (*initialGuess[2])(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void* ctx) = {coord_vector, one_scalar};
    Vec              lu;
    
    ierr = DMProjectFunction(dm, 0.0, initialGuess, NULL, INSERT_VALUES, u);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject) u, "Initial Solution");CHKERRQ(ierr);
    ierr = VecViewFromOptions(u, NULL, "-initial_vec_view");CHKERRQ(ierr);
    ierr = DMGetLocalVector(dm, &lu);CHKERRQ(ierr);
    ierr = DMPlexInsertBoundaryValues(dm, PETSC_TRUE, lu, 0.0, NULL, NULL, NULL);CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(dm, u, INSERT_VALUES, lu);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(dm, u, INSERT_VALUES, lu);CHKERRQ(ierr);
    ierr = VecViewFromOptions(lu, NULL, "-local_vec_view");CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(dm, &lu);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject) u, "Solution");CHKERRQ(ierr);
    
    /* need to get good dt */
    PetscReal dt = 0.5e0;
    DM        vdm;
    IS        vis;
    Vec       vel, locvel;
    PetscInt  vf[1] = {0};
    PetscInt  t_i, maxTimesteps;
    char      prefix[PETSC_MAX_PATH_LEN];
    PetscInt   nfields = 1;
    const char *flist[] = {"pdensity"};

    maxTimesteps = 2; t_i=0;
    for( t_i = 0; t_i < maxTimesteps; t_i++ ) {

      ierr = SwarmProjectToCells(&dm, &swarm, nfields, flist );CHKERRQ(ierr);
      PetscSNPrintf( prefix, PETSC_MAX_PATH_LEN-1, "step-%05d.xmf", t_i);
      ierr = DMSwarmViewFieldsXDMF(swarm,prefix,nfields,flist);CHKERRQ(ierr);
      ierr = SNESSolve(snes, NULL, u);CHKERRQ(ierr);

      // get sub vec for velocity and copy values to local vec
      ierr = DMCreateSubDM(dm, 1, vf, &vis, &vdm);CHKERRQ(ierr);
      ierr = DMCreateLocalVector(vdm, &locvel);CHKERRQ(ierr);
      ierr = VecGetSubVector(u, vis, &vel);CHKERRQ(ierr);
      ierr = DMGlobalToLocalBegin(vdm, vel, INSERT_VALUES, locvel);CHKERRQ(ierr);
      ierr = DMGlobalToLocalEnd(vdm, vel, INSERT_VALUES, locvel);CHKERRQ(ierr);
      VecRestoreSubVector(u,vis,&vel);

      ierr = SwarmAdvectRK1(&vdm, &locvel, &swarm, dt);

      /*
       * Must work out a way to prevent the need for this TODO: Ask Kneply
       * ierr = DMProjectFunction(dm, 0.0, initialGuess, NULL, INSERT_VALUES, u);CHKERRQ(ierr);
       * -snes_atol 1e-12
      */

    }
    PetscSNPrintf( prefix, PETSC_MAX_PATH_LEN-1, "step-%05d.xmf", t_i);
    ierr = DMSwarmViewFieldsXDMF(swarm,prefix,nfields,flist);CHKERRQ(ierr);
    ierr = VecViewFromOptions(u, NULL, "-sol_vec_view");CHKERRQ(ierr);

    ierr = VecDestroy(&locvel);CHKERRQ(ierr);
    ierr = ISDestroy(&vis);CHKERRQ(ierr);
    ierr = DMDestroy(&vdm);CHKERRQ(ierr);
  //   {
	// /* Output dm and temperature solution */
  //     PetscViewer h5viewer;
  //     PetscViewerHDF5Open(PETSC_COMM_WORLD, "sol.h5", FILE_MODE_WRITE, &h5viewer);
  //     PetscViewerSetFromOptions(h5viewer);
  //     DMView(dm, h5viewer);
  //     PetscViewerDestroy(&h5viewer);
  // 
  //     PetscViewerHDF5Open(PETSC_COMM_WORLD, "sol.h5", FILE_MODE_APPEND, &h5viewer);
  //     PetscViewerSetFromOptions(h5viewer);
  //     VecView(u, h5viewer);
  //     PetscViewerDestroy(&h5viewer);
  //   }
	      
    //VecDestroy(&nullVec);
    //MatNullSpaceDestroy(&nullSpace);
    VecDestroy(&u);
    SNESDestroy(&snes);
    DMDestroy(&dm);
    PetscFinalize();
    return 0;
}
