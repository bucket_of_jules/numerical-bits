#include <stdio.h>
#include <petsc.h>
#include <petscviewerhdf5.h>
#include <mpi.h>

/*
This example uses a 2D-DMDA to create a distributed vector.
On the vector we do a collective addition on it.
Very straight forward and elegant

command line args: -da_grid_x <M> -da_grid_y <N>
*/

typedef struct {
  char filename[2048]; /* The mesh file */
  PetscBool simplex;
} AppCtx;

static PetscErrorCode zero_scalar(PetscInt dim, PetscReal time, const PetscReal coords[], 
                                  PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = 0.0;
  return 0;
}

static PetscErrorCode fn_x(PetscInt dim, PetscReal time, const PetscReal coords[], 
                                  PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = coords[0];
  return 0;
}

static PetscErrorCode one_scalar(PetscInt dim, PetscReal time, const PetscReal coords[], 
                                 PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = 1.0;
  return 0;
}


static void f0_u(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  f0[0] = 0.;
}

static void f1_u(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  PetscInt d_i;
  for( d_i=0; d_i<dim; ++d_i ) f0[d_i] = u_x[d_i];
}

/* < \nabla v, \nabla u + {\nabla u}^T >
   This just gives \nabla u, give the perdiagonal for the transpose */
static void g3_uu(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g3[])
{
  PetscInt d_i;

  for ( d_i=0; d_i<dim; ++d_i ) { g3[d_i*dim+d_i] = 1.0; }
}

static PetscErrorCode SetupProblem(DM dm, PetscDS prob, AppCtx *user)
{
  const PetscInt          comp   = 0; /* scalar */
  PetscInt                ids[4] = {1,2,3,4};
  PetscErrorCode          ierr;

  PetscFunctionBeginUser;
  ierr = PetscDSSetResidual(prob, 0, f0_u, f1_u);CHKERRQ(ierr);
  ierr = PetscDSSetJacobian(prob, 0, 0, NULL, NULL,  NULL,  g3_uu);CHKERRQ(ierr);
  
  ierr = PetscDSAddBoundary(prob, DM_BC_ESSENTIAL, "mrJones", "wallTop", 
                            0, 0, NULL, /* field to constain and number of constained components */
                            (void (*)(void)) zero_scalar, 1, &ids[0], user);CHKERRQ(ierr);
                            
  ierr = PetscDSAddBoundary(prob, DM_BC_ESSENTIAL, "mrsSkywalker", "wallBottom", 
                            0, 0, NULL, /* field to constain and number of constained components */
                            (void (*)(void)) one_scalar, 1, &ids[0], user);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

static PetscErrorCode SetupDiscretization(DM dm, AppCtx *user)
{
  DM              cdm = dm;
  PetscFE         fe;
  //PetscQuadrature q;
  PetscDS         prob;
  PetscSpace      space;
  PetscInt        dim;
  PetscErrorCode  ierr;

  PetscFunctionBeginUser;
  ierr = DMGetDimension(dm, &dim);CHKERRQ(ierr);
  /* Create finite element */
  ierr = PetscFECreateDefault(PetscObjectComm((PetscObject)dm), dim, 1, user->simplex, "temperature_", PETSC_DEFAULT, &fe);CHKERRQ(ierr);
  ierr = PetscFEGetBasisSpace(fe, &space);CHKERRQ(ierr);
  //ierr = PetscSpaceSetOrder(space, (PetscInt)1);CHKERRQ(ierr); // set to linear
  //ierr = PetscObjectSetName((PetscObject) fe, "temperature");CHKERRQ(ierr);
  //ierr = PetscFEGetQuadrature(fe, &q);CHKERRQ(ierr);
  /* Set discretization and boundary conditions for each mesh */
  ierr = DMSetField(dm,0,NULL,(PetscObject)fe);CHKERRQ(ierr);
  ierr = DMCreateDS(dm);
  ierr = DMGetDS(dm, &prob);
  ierr = SetupProblem(dm, prob, user);CHKERRQ(ierr);
  while (cdm) {
    ierr = DMSetUp(cdm);
    ierr = DMGetCoarseDM(cdm, &cdm);CHKERRQ(ierr);
  }
  ierr = PetscFEDestroy(&fe);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc, char**argv) {

    PetscInt size;
    DM dm;
    Vec u;
    AppCtx user;
    SNES snes;
    DMLabel label;
    Vec gVec, lVec, xy;
    PetscErrorCode ierr;    

    PetscInitialize( &argc, &argv, (char*)0, NULL );

    PetscInt dim = 2;
    PetscInt elements[2] = {4,3};
    user.simplex = PETSC_FALSE;
    PetscOptionsBegin(PETSC_COMM_WORLD, "", "Julian 2D Poisson test", PETSC_NULL); // must call before options
    /* Can force termperture element type with the following
    //ierr = PetscOptionsSetValue(NULL, "-temperature_petscspace_degree", "1");CHKERRQ(ierr); // default linear trail function approximation */
    ierr=PetscOptionsBool("-simplex", "use simplicies", "n/a", user.simplex, &user.simplex, NULL);CHKERRQ(ierr);
    ierr=PetscOptionsIntArray("-elRes", "element count (default: 4,4)", "n/a", elements, &dim, NULL);CHKERRQ(ierr);
    PetscOptionsEnd();

    ierr = SNESCreate(PETSC_COMM_WORLD, &snes);CHKERRQ(ierr);
    
    PetscReal max[2]= {4.5,3};
    DMPlexCreateBoxMesh(PETSC_COMM_WORLD, 2, 
                        user.simplex, // use_simplices, if not then tesor_cells
                        elements, NULL, max, // sizes, and the min and max coords  
                        NULL, PETSC_TRUE,&dm);
    /* Distribute mesh over processes */
    {
      PetscPartitioner part;
      DM               pdm = NULL;

      ierr = DMPlexGetPartitioner(dm, &part);CHKERRQ(ierr);
      ierr = PetscPartitionerSetFromOptions(part);CHKERRQ(ierr);
      ierr = DMPlexDistribute(dm, 0, NULL, &pdm);CHKERRQ(ierr);
      if (pdm) {
        ierr = DMDestroy(&dm);CHKERRQ(ierr);
        dm  = pdm;
      }
    }
    ierr = DMLocalizeCoordinates(dm);CHKERRQ(ierr); /* needed for periodic only */
    ierr = DMSetFromOptions(dm);CHKERRQ(ierr);
    
     /* Make split wall labels - closer to uw feel must run with -dm_plex_separate_marker */
    {
      const char *names[4] = {"wallBottom", "wallRight", "wallTop", "wallLeft"};
      PetscInt    ids[4]   = {1, 2, 3, 4};
      DMLabel     label;
      IS          is;
      PetscInt    f;
    
      for (f = 0; f < 4; ++f) {
        ierr = DMGetStratumIS(dm, "marker", ids[f],  &is);CHKERRQ(ierr);
        if (!is) continue;
        ierr = DMCreateLabel(dm, names[f]);CHKERRQ(ierr);
        ierr = DMGetLabel(dm, names[f], &label);CHKERRQ(ierr);
        if (is) {
          ierr = DMLabelInsertIS(label, is, 1);CHKERRQ(ierr);
        }
        ierr = ISDestroy(&is);CHKERRQ(ierr);
      }
    }
    SetupDiscretization(dm, &user);
    /* [O] The mesh is output to HDF5 using options */
    ierr = DMViewFromOptions(dm, NULL, "-dm_view");CHKERRQ(ierr);
    ierr = DMSetApplicationContext(dm, &user);CHKERRQ(ierr);
    ierr = SNESSetDM(snes, dm); CHKERRQ(ierr);
    
    /* Calculates the index of the 'default' section, should improve performance */
    ierr = DMPlexCreateClosureIndex(dm, NULL);CHKERRQ(ierr);
    /* Sets the fem routines for boundary, residual and Jacobian point wise operations */
    ierr = DMPlexSetSNESLocalFEM(dm, &user, &user, &user);CHKERRQ(ierr);
    /* Get global vector */
    ierr = DMCreateGlobalVector(dm, &u);CHKERRQ(ierr);
    /* Update SNES */
    ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
    
    /* Solve and output*/
    {
      PetscErrorCode (*initialGuess[1])(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void* ctx) = {fn_x};
      Vec              lu;
      
      ierr = DMProjectFunction(dm, 0.0, initialGuess, NULL, INSERT_VALUES, u);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) u, "Initial Solution");CHKERRQ(ierr);
      ierr = VecViewFromOptions(u, NULL, "-initial_vec_view");CHKERRQ(ierr);
      ierr = DMGetLocalVector(dm, &lu);CHKERRQ(ierr);
      ierr = DMPlexInsertBoundaryValues(dm, PETSC_TRUE, lu, 0.0, NULL, NULL, NULL);CHKERRQ(ierr);
      ierr = DMGlobalToLocalBegin(dm, u, INSERT_VALUES, lu);CHKERRQ(ierr);
      ierr = DMGlobalToLocalEnd(dm, u, INSERT_VALUES, lu);CHKERRQ(ierr);
      ierr = VecViewFromOptions(lu, NULL, "-local_vec_view");CHKERRQ(ierr);
      ierr = DMRestoreLocalVector(dm, &lu);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) u, "Solution");CHKERRQ(ierr);
      ierr = SNESSolve(snes, NULL, u);CHKERRQ(ierr);
      ierr = VecViewFromOptions(u, NULL, "-sol_vec_view");CHKERRQ(ierr);
    }

    /*************************************
    // Output dm and temperature solution
    // to a given filename
    {
      PetscViewer h5viewer;
      PetscViewerHDF5Open(PETSC_COMM_WORLD, "sol.h5", FILE_MODE_WRITE, &h5viewer);
      PetscViewerSetFromOptions(h5viewer);
      DMView(dm, h5viewer);
      PetscViewerDestroy(&h5viewer);

      PetscViewerHDF5Open(PETSC_COMM_WORLD, "sol.h5", FILE_MODE_APPEND, &h5viewer);
      PetscViewerSetFromOptions(h5viewer);
      VecView(u, h5viewer);
      PetscViewerDestroy(&h5viewer);
    }
    **************************************/
	      

    VecDestroy(&u);
    SNESDestroy(&snes);
    DMDestroy(&dm);
    PetscFinalize();
    return 0;
}
