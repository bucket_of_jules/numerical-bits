
static char help[] = "DMSwarm-PIC demonstator of inserting points into a cell DM \n\
Options: \n\
-mode {0,1} : 0 ==> DMDA, 1 ==> DMPLEX cell DM \n\
-dim {2,3}  : spatial dimension\n";

#include <petsc.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscdmplex.h>
#include <petscdmswarm.h>
#include <petscviewerhdf5.h>

PetscErrorCode initialise_swarm(DM* _swarm) 
{
  PetscFunctionBegin;
    PetscErrorCode ierr;
    PetscReal      *coords,*eta,x,y,z;
    PetscInt       p_i,bs,lsize;
    DM             swarm = *_swarm;

    ierr = DMSwarmGetLocalSize(swarm,&lsize);CHKERRQ(ierr);
    ierr = DMSwarmGetField(swarm, DMSwarmPICField_coor, &bs, NULL, (void**)&coords );CHKERRQ(ierr);
    ierr = DMSwarmGetField(swarm, "viscosity", NULL, NULL, (void**)&eta );CHKERRQ(ierr);

    for(p_i=0;p_i<lsize;p_i++) {
        x = coords[p_i*(bs)+0];
        y = coords[p_i*(bs)+1];
        //if (dim == 3) z = coords[p_i*(bs)+2];

        //density[p_i] = 1.0-y;
        eta[p_i] = x*y;
    }
    ierr = DMSwarmRestoreField(swarm, DMSwarmPICField_coor, &bs, NULL, (void**)&coords );CHKERRQ(ierr);
    ierr = DMSwarmRestoreField(swarm, "viscosity", NULL, NULL, (void**)&eta );CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode pic_insert_DMDA(PetscInt dim)
{
  PetscErrorCode ierr;
  DM             celldm = NULL,swarm;
  PetscInt       dof,stencil_width;
  PetscReal      min[3],max[3];
  PetscInt       ndir[3];

  PetscFunctionBegin;
  /* Create the background cell DM */
  dof = 1;
  stencil_width = 1;
  if (dim == 2) {
    ierr = DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,10,8,PETSC_DECIDE,PETSC_DECIDE,dof,stencil_width,NULL,NULL,&celldm);CHKERRQ(ierr);
  }
  if (dim == 3) {
    ierr = DMDACreate3d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,25,13,19,PETSC_DECIDE,PETSC_DECIDE,PETSC_DECIDE,dof,stencil_width,NULL,NULL,NULL,&celldm);CHKERRQ(ierr);
  }

  ierr = DMDASetElementType(celldm,DMDA_ELEMENT_Q1);CHKERRQ(ierr);
  ierr = DMSetFromOptions(celldm);CHKERRQ(ierr);
  ierr = DMSetUp(celldm);CHKERRQ(ierr);

  ierr = DMDASetUniformCoordinates(celldm,-1.0,2.0,0.0,1.0,0.0,1.5);CHKERRQ(ierr);

  /* Create the DMSwarm */
  ierr = DMCreate(PETSC_COMM_WORLD,&swarm);CHKERRQ(ierr);
  ierr = DMSetType(swarm,DMSWARM);CHKERRQ(ierr);
  ierr = DMSetDimension(swarm,dim);CHKERRQ(ierr);

  /* Configure swarm to be of type PIC */
  ierr = DMSwarmSetType(swarm,DMSWARM_PIC);CHKERRQ(ierr);
  ierr = DMSwarmSetCellDM(swarm,celldm);CHKERRQ(ierr);

  /* Register two scalar fields within the DMSwarm */
  ierr = DMSwarmRegisterPetscDatatypeField(swarm,"viscosity",1,PETSC_DOUBLE);CHKERRQ(ierr);
  ierr = DMSwarmRegisterPetscDatatypeField(swarm,"density",1,PETSC_DOUBLE);CHKERRQ(ierr);
  ierr = DMSwarmFinalizeFieldRegister(swarm);CHKERRQ(ierr);

  /* Set initial local sizes of the DMSwarm with a buffer length of zero */
  ierr = DMSwarmSetLocalSizes(swarm,4,0);CHKERRQ(ierr);

  /* Insert swarm coordinates cell-wise */
  ierr = DMSwarmInsertPointsUsingCellDM(swarm,DMSWARMPIC_LAYOUT_REGULAR,3);CHKERRQ(ierr);
  /*
  min[0] = 0.5; max[0] = 0.7;
  min[1] = 0.5; max[1] = 0.8;
  min[2] = 0.5; max[2] = 0.9;
  ndir[0] = ndir[1] = ndir[2] = 30;
  ierr = DMSwarmSetPointsUniformCoordinates(swarm,min,max,ndir,ADD_VALUES);CHKERRQ(ierr);
  */

  initialise_swarm(&swarm);

  /* This should be dispatched from a regular DMView() */
  ierr = DMSwarmViewXDMF(swarm,"swarm.xmf");CHKERRQ(ierr);
  ierr = DMView(celldm,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = DMView(swarm,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  {
    PetscInt    npoints,*list;
    PetscMPIInt rank;

    ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
    ierr = DMSwarmSortGetAccess(swarm);CHKERRQ(ierr);
    ierr = DMSwarmSortGetNumberOfPointsPerCell(swarm,0,&npoints);CHKERRQ(ierr);
    ierr = DMSwarmSortGetPointsPerCell(swarm,rank,&npoints,&list);CHKERRQ(ierr);
    ierr = PetscFree(list);CHKERRQ(ierr);
    ierr = DMSwarmSortRestoreAccess(swarm);CHKERRQ(ierr);
  }
  { /* Projection Code */
    const char *fieldnames[] = {"viscosity"};
    Vec        *fields;

    ierr = DMSwarmProjectFields( swarm,1,fieldnames,&fields,PETSC_FALSE);
    {
        PetscViewer viewer;
        PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
        PetscViewerSetType(viewer, PETSCVIEWERVTK);
        PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);
        PetscViewerFileSetName(viewer,"projection.vts");
        VecView(fields[0],viewer);
        PetscViewerDestroy(&viewer);
    }
    VecDestroy(&fields[0]);
    PetscFree(fields);
  }

  ierr = DMSwarmMigrate(swarm,PETSC_FALSE);CHKERRQ(ierr);
  ierr = DMDestroy(&celldm);CHKERRQ(ierr);
  ierr = DMDestroy(&swarm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode vecPrint(Vec* vec, PetscInt t_i) {
  char prefix[PETSC_MAX_PATH_LEN];
  PetscViewer h5viewer;
  DM dm;

  PetscSNPrintf( prefix, PETSC_MAX_PATH_LEN-1, "proj-%05d.h5", t_i);

  // need to include geometry in .h5 - is wasteful
  VecGetDM(*vec, &dm); // TODO: do i need to dealloc dm?
  PetscViewerHDF5Open(PETSC_COMM_WORLD, prefix, FILE_MODE_WRITE, &h5viewer);
  PetscViewerSetFromOptions(h5viewer);
  DMView(dm, h5viewer);
  PetscViewerDestroy(&h5viewer);

  PetscViewerHDF5Open(PETSC_COMM_WORLD, prefix, FILE_MODE_APPEND, &h5viewer);
  PetscViewerSetFromOptions(h5viewer);
  VecView(*vec, h5viewer);
  PetscViewerDestroy(&h5viewer);
}

PetscErrorCode pic_insert_DMPLEX(PetscBool is_simplex,PetscInt dim)
{
  PetscErrorCode ierr;
  DM             celldm,swarm,distributedMesh = NULL;
  const char     *fieldnames[] = {"viscosity","DMSwarm_rank"};

  PetscFunctionBegin;

  /* Create the background cell DM */
  {
    PetscInt faces[3] = {4, 2, 4};
    ierr = DMPlexCreateBoxMesh(PETSC_COMM_WORLD, dim, is_simplex, faces, NULL, NULL, NULL, PETSC_TRUE, &celldm);CHKERRQ(ierr);
  }

  /* Distribute mesh over processes */
  ierr = DMPlexDistribute(celldm,0,NULL,&distributedMesh);CHKERRQ(ierr);
  if (distributedMesh) {
    ierr = DMDestroy(&celldm);CHKERRQ(ierr);
    celldm = distributedMesh;
  }
  ierr = DMSetFromOptions(celldm);CHKERRQ(ierr);
  {
    PetscInt     numComp[] = {1};
    PetscInt     numDof[] = {1,0,0}; /* vert, edge, cell */
    PetscInt     numBC = 0;
    PetscSection section;

    ierr = DMPlexCreateSection(celldm,dim,1,numComp,numDof,numBC,NULL,NULL,NULL,NULL,&section);CHKERRQ(ierr);
    ierr = DMSetDefaultSection(celldm,section);CHKERRQ(ierr);
    ierr = PetscSectionDestroy(&section);CHKERRQ(ierr);
  }
  ierr = DMSetUp(celldm);CHKERRQ(ierr);
  {
    PetscViewer viewer;

    ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
    ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
    ierr = PetscViewerFileSetName(viewer,"dm_decomp.vtk");CHKERRQ(ierr);
    ierr = DMView(celldm,viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }

  ierr = DMCreate(PETSC_COMM_WORLD,&swarm);CHKERRQ(ierr);
  ierr = DMSetType(swarm,DMSWARM);CHKERRQ(ierr);
  ierr = DMSetDimension(swarm,dim);CHKERRQ(ierr);

  ierr = DMSwarmSetType(swarm,DMSWARM_PIC);CHKERRQ(ierr);
  ierr = DMSwarmSetCellDM(swarm,celldm);CHKERRQ(ierr);

  /* Register two scalar fields within the DMSwarm */
  ierr = DMSwarmRegisterPetscDatatypeField(swarm,"viscosity",1,PETSC_DOUBLE);CHKERRQ(ierr);
  ierr = DMSwarmRegisterPetscDatatypeField(swarm,"density",1,PETSC_DOUBLE);CHKERRQ(ierr);
  ierr = DMSwarmFinalizeFieldRegister(swarm);CHKERRQ(ierr);

  /* Set initial local sizes of the DMSwarm with a buffer length of zero */
  ierr = DMSwarmSetLocalSizes(swarm,4,0);CHKERRQ(ierr);

  /* Insert swarm coordinates cell-wise */
  ierr = DMSwarmInsertPointsUsingCellDM(swarm,DMSWARMPIC_LAYOUT_GAUSS,3);CHKERRQ(ierr);
  initialise_swarm(&swarm);

  PetscObjectViewFromOptions((PetscObject)celldm,NULL,"-dm_view");
  { /* Projection Code */
    const char *fieldnames[] = {"viscosity"};
    Vec        *fields;

    ierr = DMSwarmProjectFields( swarm,1,fieldnames,&fields,PETSC_FALSE);
    vecPrint( &fields[0], 0 );
    //PetscObjectViewFromOptions((PetscObject)fields[0],NULL,"-vec_view");
    VecDestroy(&fields[0]);
    PetscFree(fields);
  }
  ierr = DMSwarmViewFieldsXDMF(swarm,"swarm.xmf",2,fieldnames);CHKERRQ(ierr);

  /* Some ascii outputs */
  ierr = DMView(celldm,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = DMView(swarm,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = DMDestroy(&celldm);CHKERRQ(ierr);
  ierr = DMDestroy(&swarm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **args)
{
  PetscErrorCode ierr;
  PetscInt       mode = 0;
  PetscInt       dim = 2;

  ierr = PetscInitialize(&argc,&args,(char*)0,help);if (ierr) return ierr;
  ierr = PetscOptionsGetInt(NULL,NULL,"-mode",&mode,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-dim",&dim,NULL);CHKERRQ(ierr);
  switch (mode) {
    case 0:
      ierr = pic_insert_DMDA(dim);CHKERRQ(ierr);
      break;
    case 1:
      /* tri / tet */
      ierr = pic_insert_DMPLEX(PETSC_TRUE,dim);CHKERRQ(ierr);
      break;
    case 2:
      /* quad / hex */
      ierr = pic_insert_DMPLEX(PETSC_FALSE,dim);CHKERRQ(ierr);
      break;
    default:
      ierr = pic_insert_DMDA(dim);CHKERRQ(ierr);
      break;
  }
  ierr = PetscFinalize();
  return ierr;
}

/*TEST

   test:
      args:
      requires: !complex double
      filter: grep -v DM_ | grep -v atomic
      filter_output: grep -v atomic

   test:
      suffix: 2
      requires: triangle double !complex
      args: -mode 1
      filter: grep -v DM_ | grep -v atomic
      filter_output: grep -v atomic

TEST*/
