#include <stdio.h>
#include <petsc.h>
#include <petscdm.h>
#include <petscds.h>
#include <petscfe.h>
#include <petscdmswarm.h>
#include <petscviewerhdf5.h>
#include <mpi.h>

/*
This example uses a DMPlex to create a distributed vector.
On the vector we do a collective addition on it.
Very straight forward and elegant
*/

typedef struct {
  PetscBool simplex;
  PetscInt  dim;
  PetscInt  elements[3];
} AppCtx; 

PetscErrorCode ProcessOptions(MPI_Comm, AppCtx* );

static PetscErrorCode init_vector(PetscInt dim, PetscReal time, const PetscReal coords[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  PetscInt i;
  for( i = dim-1; i>=0; i-- ) { 
    u[i] = PetscSinReal(2*PETSC_PI*coords[i]);
  }
  return 0;
}

static PetscErrorCode init_scalar(PetscInt dim, PetscReal time, const PetscReal coords[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  PetscInt i;
  u[0] = 1;
  for( i = dim-1; i>=0; i-- ) { 
    u[0] *= PetscSinReal(2*PETSC_PI*coords[i]);
  }
  return 0;
}

static PetscErrorCode SetupSwarm(DM* celldm, DM* swarm) {
  PetscErrorCode ierr;
  PetscInt       dim;

  PetscFunctionBeginUser;

  ierr = DMGetDimension(*celldm, &dim);CHKERRQ(ierr);
  ierr = DMCreate(PETSC_COMM_WORLD,swarm);CHKERRQ(ierr);
  ierr = DMSetType(*swarm,DMSWARM);CHKERRQ(ierr);
  ierr = DMSetDimension(*swarm,dim);CHKERRQ(ierr);

  ierr = DMSwarmSetType(*swarm,DMSWARM_PIC);CHKERRQ(ierr);
  ierr = DMSwarmSetCellDM(*swarm,*celldm);CHKERRQ(ierr);

  /* Register two scalar fields within the DMSwarm */
  ierr = DMSwarmRegisterPetscDatatypeField(*swarm,"pdensity",1,PETSC_DOUBLE);CHKERRQ(ierr);
  ierr = DMSwarmFinalizeFieldRegister(*swarm);CHKERRQ(ierr);

  /* Set initial local sizes of the DMSwarm with a buffer length of zero */
  ierr = DMSwarmSetLocalSizes(*swarm,4,0);CHKERRQ(ierr);

  /* Insert swarm coordinates cell-wise */
  ierr = DMSwarmInsertPointsUsingCellDM(*swarm,DMSWARMPIC_LAYOUT_GAUSS,2);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

static PetscErrorCode initialise_swarm(DM* _swarm) 
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscReal      *coords,*rho,x,y,z;
  PetscInt       dim, p_i,bs,lsize;
  DM             swarm = *_swarm;

  ierr = DMGetDimension(swarm,&dim);CHKERRQ(ierr);
  ierr = DMSwarmGetLocalSize(swarm,&lsize);CHKERRQ(ierr);
  ierr = DMSwarmGetField(swarm, DMSwarmPICField_coor, &bs, NULL, (void**)&coords );CHKERRQ(ierr);
  ierr = DMSwarmGetField(swarm, "pdensity", NULL, NULL, (void**)&rho );CHKERRQ(ierr);

  for(p_i=0;p_i<lsize;p_i++) {
    x = coords[p_i*(bs)+0];
    y = coords[p_i*(bs)+1];
    if (dim == 3) z = coords[p_i*(bs)+2];

    rho[p_i] = PetscSinReal(2*PETSC_PI*x)-PetscCosReal(2*PETSC_PI*y);
  }
  ierr = DMSwarmRestoreField(swarm, DMSwarmPICField_coor, &bs, NULL, (void**)&coords );CHKERRQ(ierr);
  ierr = DMSwarmRestoreField(swarm, "pdensity", NULL, NULL, (void**)&rho );CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


int main(int argc, char**argv) {

    int size;
    PetscInt faces[3] = {10,8,7}; /* only needs 2 in 2D */
    PetscInt dim=0;
    DM da, dmDist, swarm;
    PetscFE fe[2];
    PetscViewer v;
    PetscDS prob;
    PetscQuadrature q;
    Vec gVec, lVec, xy;
    PetscErrorCode ierr;
    PetscErrorCode  (*initialGuess[2])(PetscInt dim, PetscReal time, 
                    const PetscReal x[], PetscInt Nf, PetscScalar *u, void* ctx) = {init_vector, init_scalar};
    AppCtx user;

    ierr = PetscInitialize(&argc, &argv, NULL,NULL);if (ierr) return ierr;
    ierr = ProcessOptions(PETSC_COMM_WORLD, &user);CHKERRQ(ierr);

    ierr = DMPlexCreateBoxMesh(PETSC_COMM_WORLD, 
        user.dim, user.simplex, user.elements, 
        NULL, NULL, NULL, PETSC_TRUE, &da);CHKERRQ(ierr);

    /* Distribute mesh over processes - no Partitioning as in previous examples */
    ierr = DMPlexDistribute(da, 0, NULL, &dmDist);CHKERRQ(ierr);
    if (dmDist) {
      ierr = PetscObjectSetName((PetscObject)dmDist,"Distributed Mesh");CHKERRQ(ierr);
      ierr = DMDestroy(&da);CHKERRQ(ierr);
      da  = dmDist;
    }

    DMSetFromOptions(da);
    DMSetUp(da);
    ierr = DMGetDimension(da, &dim);CHKERRQ(ierr);

    PetscFECreateDefault(PETSC_COMM_WORLD, dim, dim, user.simplex, "v_", PETSC_DECIDE, &fe[0]);
    PetscObjectSetName((PetscObject) fe[0], "velocity");

    PetscFECreateDefault(PETSC_COMM_WORLD, dim, 1, user.simplex, "p_", PETSC_DECIDE, &fe[1]);
    PetscObjectSetName((PetscObject) fe[1], "pressure");

    /* Setting the Discretization is required to save to hdf5, presumably this initialises behind the scenes */
    DMGetDS(da, &prob);
    PetscDSSetDiscretization(prob, 0, (PetscObject) fe[0]);
    PetscDSSetDiscretization(prob, 1, (PetscObject) fe[1]);
    DMSetDS(da, prob);
    //
    // create global and local vectors
    DMCreateGlobalVector(da, &gVec);
    DMCreateLocalVector(da, &lVec);

    // initialise fem vars
    DMProjectFunction(da, 0.0, initialGuess, NULL, INSERT_ALL_VALUES, gVec);

    /***
     * Now build the entire data structure for an 'auxilliary' field approach
     * to use in PetscDS
     ***/
    DM da2;
    PetscDS probAux;
    PetscFE projFE;
    Vec projV;
    ierr = DMClone(da, &da2);CHKERRQ(ierr);
    DMSetFromOptions(da2);
    DMSetUp(da2);

    PetscFECreateDefault(PETSC_COMM_WORLD, dim, 1, user.simplex, "smooth", PETSC_DECIDE, &projFE);
    PetscObjectSetName((PetscObject) projFE, "smooth density");

    DMGetDS(da2, &probAux);
    PetscDSSetDiscretization(probAux, 0, (PetscObject) projFE);
    DMSetDS(da2, probAux);

    DMCreateGlobalVector(da2, &projV);

    ierr = SetupSwarm(&da2, &swarm);CHKERRQ(ierr);
    ierr = initialise_swarm(&swarm);

    PetscObjectSetName((PetscObject) gVec, "global");
    PetscObjectViewFromOptions((PetscObject)da, NULL, "-dm_view");
    PetscObjectViewFromOptions((PetscObject)da2, NULL, "-dm2_view");
    PetscObjectViewFromOptions((PetscObject)gVec, NULL, "-vec_view");

    Vec *list = &projV;
    {
      const char filename[] = "swarm.xmf";
      PetscInt   nfields = 1;
      const char *flist[] = {"pdensity"}; 
      DMSwarmViewFieldsXDMF(swarm,filename,nfields,flist);
      ierr = DMSwarmProjectFields(swarm,nfields,flist,&list,PETSC_TRUE);
      PetscObjectSetName((PetscObject) projV, "proj");
      PetscObjectViewFromOptions((PetscObject)projV, NULL, "-pro_view");
      VecDestroy(&projV);
    }

    /* C-Code for hdf5 output
    PetscViewerHDF5Open(PetscObjectComm((PetscObject) da), "dm.h5", FILE_MODE_WRITE, &v);
    DMView(da,v);
    PetscViewerDestroy(&v);

    PetscViewerHDF5Open(PetscObjectComm((PetscObject) gVec), "aux.h5", FILE_MODE_READ, &v);
    VecLoad(gVec, v);
    PetscViewerDestroy(&v);
    VecView(gVec,PETSC_VIEWER_STDOUT_WORLD);
    PetscPrintf(PETSC_COMM_WORLD, "***Print coordinates:\n");
    DMGetCoordinates(da,&xy);
    */

    // clean up
    VecDestroy(&gVec);VecDestroy(&lVec);
    PetscFEDestroy(&fe[0]);
    PetscFEDestroy(&fe[1]);
    DMDestroy(&da);
    DMDestroy(&da2);
    DMDestroy(&swarm);
    PetscFinalize();
    return 0;
}

PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *options)
{
  PetscInt       sol;
  PetscBool      specified;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  options->elements[0]     = 10;
  options->elements[1]     = 8;
  options->elements[2]     = 7;
  options->simplex         = PETSC_FALSE;

  ierr = PetscOptionsBegin(comm, "", "My options", "DMPLEX");CHKERRQ(ierr);
  ierr = PetscOptionsIntArray("-elRes", "element count (default: 5,5,5)", "n/a", options->elements, &options->dim, &specified);
  if(!specified) options->dim=2;

  ierr = PetscOptionsBool("-simplex", "Use simplices or tensor product cells", "ex69.c", options->simplex, &options->simplex, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();
  PetscFunctionReturn(0);
}

