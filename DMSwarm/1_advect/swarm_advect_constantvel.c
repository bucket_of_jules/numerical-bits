
static char help[] = "DMSwarm-PIC demonstator of inserting points into a cell DM \n\
Options: \n\
-dim {2,3}      : spatial dimension\n\
-ppcell -2      : particles per cell, -ve number is for guass point distribution\n\
-use_plex 1     : Use plex or dmda\n\
-simplex 0      : Use simplicies for the vp mesh\n";

#include <petsc.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscdmplex.h>
#include <petscdmswarm.h>
#include <petscviewerhdf5.h>

PetscErrorCode SwarmProjectAndPrint( DM* swarm, PetscBool use_plex, const char *pfieldnames[], PetscInt pnfields, const char *idstr );


PetscErrorCode init_vector(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = -1 * (x[1] - 0.5);
  u[1] =      (x[0] - 0.5);
  return 0;
}

PetscErrorCode init_scalar(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = PetscSqrtReal( (x[1]-0.5)*(x[1]-0.5)+(x[0]-0.5)*(x[0]-0.5) );
  return 0;
}

PetscErrorCode DMBuildVelocityField(DM* _dm, PetscBool is_simplex) {

  PetscFunctionBegin;
  /* create a vector field for the veloicty which will advect the swarm */
  PetscDS ds;
  PetscFE fe[2];
  Vec vec;
  PetscInt dim;
  DM* dm = _dm;
  //PetscErrorCode ierr, (*initialGuess[1])(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void* ctx) = {init_vector};

  PetscErrorCode ierr, (*initialGuess[2])(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void* ctx) = {init_vector, init_scalar};
  PetscQuadrature q;
  MPI_Comm comm;

  ierr = PetscObjectGetComm((PetscObject) *dm, &comm);CHKERRQ(ierr);
  // build velocity PetscFE
  ierr = DMGetDimension(*dm, &dim);
  ierr = DMGetDS(*dm, &ds);CHKERRQ(ierr);
  ierr = PetscFECreateDefault(comm, dim, dim, is_simplex, "vel_", PETSC_DEFAULT, &fe[0]);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe[0], "velocity");CHKERRQ(ierr);
  ierr = PetscFEGetQuadrature(fe[0], &q);CHKERRQ(ierr);
  ierr = PetscDSSetDiscretization(ds, 0, (PetscObject)fe[0]);CHKERRQ(ierr);
  ierr = PetscFEDestroy(&fe[0]);CHKERRQ(ierr); // can do because fe[0] is associated with DS

  ierr = PetscFECreateDefault(comm, dim, 1, is_simplex, "pres_", PETSC_DEFAULT, &fe[1]);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe[1], "pressure");CHKERRQ(ierr);
  ierr = PetscFESetQuadrature(fe[1], q);CHKERRQ(ierr);
  ierr = PetscDSSetDiscretization(ds, 1, (PetscObject)fe[1]);CHKERRQ(ierr);
  ierr = PetscFEDestroy(&fe[1]);CHKERRQ(ierr); // can do because fe[0] is associated with DS

  // initialise velocity with solid body rotation vector
  ierr = DMCreateGlobalVector(*dm, &vec);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) vec, "solution");CHKERRQ(ierr);
  ierr = DMProjectFunction(*dm, 0.0, initialGuess, NULL, INSERT_VALUES, vec);CHKERRQ(ierr);

  ierr = PetscObjectViewFromOptions((PetscObject)(vec), NULL, "-vec_view");CHKERRQ(ierr);

  PetscInt size;
    VecGetLocalSize(vec, &size);
    PetscReal norm;
  VecNorm(vec,NORM_2,&norm);
  printf("The size/norm %d, %f\n", size, norm);

  ierr = VecDestroy(&vec);
  PetscFunctionReturn(0);
}

PetscErrorCode vecPrint(Vec* vec, const char *idstr) {
  PetscViewer h5viewer;
  DM dm;

  // need to include geometry in .h5 - is wasteful
  VecGetDM(*vec, &dm); // TODO: do i need to dealloc dm?
  PetscViewerHDF5Open(PETSC_COMM_WORLD, idstr, FILE_MODE_WRITE, &h5viewer);
  PetscViewerSetFromOptions(h5viewer);
  DMView(dm, h5viewer);
  PetscViewerDestroy(&h5viewer);

  PetscViewerHDF5Open(PETSC_COMM_WORLD, idstr, FILE_MODE_APPEND, &h5viewer);
  PetscViewerSetFromOptions(h5viewer);
  VecView(*vec, h5viewer);
  PetscViewerDestroy(&h5viewer);
}

PetscErrorCode SwarmAdvectRK1(DM celldm, Vec gridvel, DM swarm, PetscReal dt) {
  /**
   * Advects a DMSwarm, dt in time, using a velocity vector from another DM
   **/
  PetscFunctionBegin;
  DMInterpolationInfo ipInfo;
  Vec                 pvel;
  PetscReal           *coords;
  PetscInt            lsize, p_i, dim, blockSize;
  const PetscScalar   *pv;
  PetscErrorCode      ierr;

  /* get a vector fields on the swarm */
  ierr = DMSwarmVectorDefineField(swarm, DMSwarmPICField_coor);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(swarm, &pvel);CHKERRQ(ierr);
  ierr = DMSwarmGetLocalSize(swarm, &lsize);CHKERRQ(ierr);

  // create interpolation
  ierr = DMInterpolationCreate(PetscObjectComm((PetscObject)celldm), &ipInfo);
  ierr = DMGetDimension(celldm, &dim);
  ierr = DMInterpolationSetDim(ipInfo, dim);
  ierr = DMInterpolationSetDof(ipInfo, dim);

  // add points
  ierr = DMSwarmGetField(swarm, DMSwarmPICField_coor, NULL, NULL, (void **) &coords);CHKERRQ(ierr);
  ierr = DMInterpolationAddPoints(ipInfo, lsize, coords);CHKERRQ(ierr);
  ierr = DMSwarmRestoreField(swarm, DMSwarmPICField_coor, NULL, NULL, (void **) &coords);CHKERRQ(ierr);

  ierr = DMInterpolationSetUp(ipInfo, celldm, PETSC_FALSE);CHKERRQ(ierr);
  ierr = DMInterpolationEvaluate(ipInfo, celldm, gridvel, pvel);CHKERRQ(ierr);
  ierr = DMInterpolationDestroy(&ipInfo);CHKERRQ(ierr);

  // update locations of points
  ierr = DMSwarmGetField(swarm, DMSwarmPICField_coor, &blockSize, NULL, (void**)&coords);CHKERRQ(ierr);
  ierr = VecGetArrayRead(pvel, &pv);CHKERRQ(ierr);
  for(p_i=0; p_i<lsize; p_i++) {
    coords[p_i*(dim)+0] += pv[p_i*blockSize+0] * dt;
    coords[p_i*(dim)+1] += pv[p_i*blockSize+1] * dt;
  }
  ierr = VecRestoreArray(pvel, (PetscScalar**)&pv);CHKERRQ(ierr);
  ierr = DMSwarmRestoreField(swarm, DMSwarmPICField_coor, NULL, NULL, (void**)&coords);CHKERRQ(ierr);
  ierr = DMSwarmMigrate(swarm, PETSC_TRUE);CHKERRQ(ierr);
  ierr = VecDestroy(&pvel);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode pic_insert_DMPLEX(PetscBool use_plex, PetscBool is_simplex, PetscInt steps, PetscInt dim, PetscInt ppcell)
{
  PetscErrorCode ierr;
  DM             celldm,swarm,swarmcelldm,distributedMesh = NULL;
  const char     *fieldnames[] = {"viscosity","DMSwarm_rank","rank0"};
  PetscInt faces[2] = {10, 10};

  PetscFunctionBegin;

  /* Create the background cell DM */
  if(use_plex) {

    ierr = DMPlexCreateBoxMesh(PETSC_COMM_WORLD, dim, is_simplex, faces, 
            NULL, NULL, NULL, PETSC_TRUE, &celldm);CHKERRQ(ierr);

    /* Distribute mesh over processes */
    ierr = DMPlexDistribute(celldm,0,NULL,&distributedMesh);CHKERRQ(ierr);
    if (distributedMesh) {
        ierr = DMDestroy(&celldm);CHKERRQ(ierr);
        celldm = distributedMesh;
    }
  } else {

    PetscInt stencil_width=1,dof=2;
    ierr = DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,
            10,8,PETSC_DECIDE,PETSC_DECIDE,dof,stencil_width,NULL,NULL,&celldm);CHKERRQ(ierr);
  }

  ierr = DMSetFromOptions(celldm);CHKERRQ(ierr);
  ierr = DMSetUp(celldm);CHKERRQ(ierr);

  // get the number of cells from the mesh dm
  PetscInt nel;
  if(use_plex) {
      PetscInt eStart,eEnd;
      ierr = DMPlexGetHeightStratum(celldm, 0, &eStart, &eEnd);CHKERRQ(ierr);
      nel = eEnd - eStart;
  } else {
      PetscInt ne,nen;
      const PetscInt *elist;
      ierr = DMDAGetElements(celldm, &ne, &nen, &elist);
      nel  = ne;
      ierr = DMDARestoreElements(celldm, &ne, &nen, &elist);

      // must set coordinates on the mesh manually
      ierr = DMDASetUniformCoordinates(celldm,0.0,1,0.0,1.0,0.0,0.0);CHKERRQ(ierr);
  }
  /**********************************************************/

  /**********************************************************/
  /* View the mesh */
  if (use_plex) {
    ierr = DMViewFromOptions(celldm, NULL, "-dm_view");CHKERRQ(ierr);
  } else {
    /* take home message is you can't view dmda in hdf5 as easily as dmplex */

    printf("***WARNING*** DMDA OUTPUT IN HDF5 HASN'T BEEN IMPLEMENTED\n");
    PetscViewer v;
    Vec gVec;
    PetscViewerHDF5Open(PetscObjectComm((PetscObject) celldm), "dm.h5", FILE_MODE_WRITE, &v);
    DMCreateGlobalVector(celldm, &gVec);
    VecSet(gVec, 0.);
    PetscObjectView((PetscObject)celldm,v);
    PetscViewerDestroy(&v);
    VecDestroy(&gVec);
  }
  /**********************************************************/

  /* build velocity and solution vectors */
  DMBuildVelocityField(&celldm, is_simplex);

   
  /**********************************************************/
#if 0
  /* For a simplex swarmcelldm, when using tensor-product celldm
     I couldn't get this to work when particles left the box?
   */
  faces[0]=faces[1]=10;
  ierr = DMPlexCreateBoxMesh(PETSC_COMM_WORLD, dim, PETSC_TRUE, faces, 
              NULL, NULL, NULL, PETSC_FALSE, &swarmcelldm);CHKERRQ(ierr);
  /* Distribute mesh over processes */
  ierr = DMPlexDistribute(swarmcelldm,0,NULL,&distributedMesh);CHKERRQ(ierr);
  if (distributedMesh) {
      ierr = DMDestroy(&swarmcelldm);CHKERRQ(ierr);
      swarmcelldm = distributedMesh;
  }
  ierr = DMSetUp(swarmcelldm);CHKERRQ(ierr);
#endif

  /* Create the swarm */
  ierr = DMClone(celldm,&swarmcelldm);CHKERRQ(ierr);
  {
    PetscInt     numComp[] = {1};
    PetscInt     numDof[] = {1,0,0}; /* vert, edge, cell */
    PetscInt     numBC = 0;
    PetscSection section;

    ierr = DMPlexCreateSection(swarmcelldm,dim,1,numComp,numDof,numBC,NULL,NULL,NULL,NULL,&section);CHKERRQ(ierr);
    ierr = DMSetDefaultSection(swarmcelldm,section);CHKERRQ(ierr);
    ierr = PetscSectionDestroy(&section);CHKERRQ(ierr);
  }

  ierr = DMCreate(PETSC_COMM_WORLD,&swarm);CHKERRQ(ierr);
  ierr = DMSetType(swarm,DMSWARM);CHKERRQ(ierr);
  ierr = DMSetDimension(swarm,dim);CHKERRQ(ierr);

  ierr = DMSwarmSetType(swarm,DMSWARM_PIC);CHKERRQ(ierr);
  ierr = DMSwarmSetCellDM(swarm,swarmcelldm);CHKERRQ(ierr);

  /* Register two scalar fields within the DMSwarm */
  ierr = DMSwarmRegisterPetscDatatypeField(swarm,fieldnames[0],1,PETSC_DOUBLE);CHKERRQ(ierr);
  ierr = DMSwarmRegisterPetscDatatypeField(swarm,fieldnames[2],1,PETSC_INT);CHKERRQ(ierr);
  ierr = DMSwarmFinalizeFieldRegister(swarm);CHKERRQ(ierr);

  if (ppcell < 0) {
      /* Set initial local sizes of the DMSwarm with a buffer length of zero */
      ierr = DMSwarmSetLocalSizes(swarm,nel*abs(ppcell),0);CHKERRQ(ierr);
      /* Insert swarm coordinates cell-wise */
      ierr = DMSwarmInsertPointsUsingCellDM(swarm,DMSWARMPIC_LAYOUT_GAUSS,abs(ppcell));CHKERRQ(ierr);
  } else {
      ierr = DMSwarmSetLocalSizes(swarm, nel*ppcell, 250 );
      ierr = DMSwarmInsertPointsUsingCellDM(swarm,DMSWARMPIC_LAYOUT_SUBDIVISION,ppcell);CHKERRQ(ierr);
  }
  PetscInt size, lsize;
  PetscMPIInt rank;
  MPI_Comm_rank( PETSC_COMM_WORLD, &rank );
  ierr = DMSwarmGetSize(swarm, &size);CHKERRQ(ierr);
  ierr = DMSwarmGetLocalSize(swarm, &lsize);CHKERRQ(ierr);
  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,
          "rank %d DMSwarm size & local size: %d %d\n",
          rank, size, lsize );CHKERRQ(ierr);
  ierr = PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);CHKERRQ(ierr);
  /* Setup viscosity for each point */
  {
    PetscReal *coords,*eta,x,y,z;
    PetscInt   p_i,bs, *rank0;
    /* DMSwarmPICField_coord is a special field registered with DMSwarm during DMSwarmSetType() 
     * Special fields are DMSwarmField_pid, DMSwarmField_rank, DMSwarmPICField_coor, DMSwarmPICField_cellid*/
    ierr = DMSwarmGetField(swarm, DMSwarmPICField_coor, &bs, NULL, (void**)&coords );CHKERRQ(ierr);
    ierr = DMSwarmGetField(swarm, fieldnames[0], NULL, NULL, (void**)&eta );CHKERRQ(ierr);
    ierr = DMSwarmGetField(swarm, fieldnames[2], NULL, NULL, (void**)&rank0 );CHKERRQ(ierr);
    /*
    */
    for(p_i=0;p_i<lsize;p_i++) {
        x = coords[p_i*(bs)+0];
        y = coords[p_i*(bs)+1];
        if (dim == 3) z = coords[p_i*(bs)+2];

        rank0[p_i] = rank;
        eta[p_i] = PetscCosReal(4*PETSC_PI*x)*PetscCosReal(2*PETSC_PI*y);
    }
    ierr = DMSwarmRestoreField(swarm, DMSwarmPICField_coor, &bs, NULL, (void**)&coords );CHKERRQ(ierr);
    ierr = DMSwarmRestoreField(swarm, fieldnames[0], NULL, NULL, (void**)&eta );CHKERRQ(ierr);
    ierr = DMSwarmRestoreField(swarm, fieldnames[2], NULL, NULL, (void**)&rank0 );CHKERRQ(ierr);
  }


  {
    /* Time loop */
    PetscInt   p_i, blockSize, t_i;
    PetscReal *coord, vx, vy, dt = 0.01;
    char prefix[PETSC_MAX_PATH_LEN];


    DM        vdm;
    IS        vis;
    Vec       u,vel, locvel;
    PetscInt  vf[1] = {0};

    /*
     * Idea: Get the global vector, get the velocity part, get the local part of that
     */
    // get global vec and create local
    ierr = DMGetGlobalVector(celldm, &u);CHKERRQ(ierr);

    // get sub vec for velocity and copy values to local vec
    ierr = DMCreateSubDM(celldm, 1, vf, &vis, &vdm);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(vdm, &locvel);CHKERRQ(ierr);
    ierr = VecGetSubVector(u, vis, &vel);CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(vdm, vel, INSERT_VALUES, locvel);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(vdm, vel, INSERT_VALUES, locvel);CHKERRQ(ierr);
    // delete global vectors
    VecRestoreSubVector(u,vis,&vel);
    DMRestoreGlobalVector(celldm,&u);

    PetscInt size;
    VecGetLocalSize(locvel, &size);
    PetscReal norm;
    VecNorm(locvel,NORM_2,&norm);
    printf("The size/norm %d, %f\n", size, norm);

    for(t_i=0; t_i<steps; t_i++) {
      // save current timestep of particles
      PetscPrintf( PETSC_COMM_WORLD, "At step %05d\n", t_i);
      PetscSNPrintf( prefix, PETSC_MAX_PATH_LEN-1, "step-%05d.xmf", t_i);
      ierr = DMSwarmViewFieldsXDMF(swarm,prefix,3,fieldnames);CHKERRQ(ierr);

      { /* Projection of fields */
        const char *pfieldnames[] = {"viscosity"};
        PetscInt pnfields = 1;
        char idstr[PETSC_MAX_PATH_LEN];
        PetscSNPrintf( idstr, PETSC_MAX_PATH_LEN-1, "proj-%05d.h5", t_i);
        SwarmProjectAndPrint( &swarm, use_plex, pfieldnames, pnfields, &(idstr[0]) );
      }


      SwarmAdvectRK1( vdm, locvel, swarm, dt );
    }
    VecDestroy(&locvel);
  }

  ierr = DMViewFromOptions(swarm,NULL,"-swarm_view");CHKERRQ(ierr);

  ierr = DMDestroy(&swarmcelldm);CHKERRQ(ierr);
  ierr = DMDestroy(&celldm);CHKERRQ(ierr);
  ierr = DMDestroy(&swarm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode SwarmProjectAndPrint( DM* swarm, PetscBool use_plex, const char *pfieldnames[], PetscInt pnfields, const char *idstr ) {

  PetscErrorCode ierr;
  Vec *fields;
  /* TODO: Waste full with REUSE as FALSE */
  ierr = DMSwarmProjectFields( *swarm,pnfields,pfieldnames,&fields,PETSC_FALSE);

  if(!use_plex) { 
    PetscViewer viewer;
    PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
    PetscViewerSetType(viewer, PETSCVIEWERVTK);
    PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);
    PetscViewerFileSetName(viewer,"visco.vts");
    VecView(fields[0],viewer);
    PetscViewerDestroy(&viewer);
  } else {
    vecPrint( &fields[0], idstr );
  }
  VecDestroy(&fields[0]);
  PetscFree(fields);
 
  PetscFunctionReturn(0);
}

int main(int argc,char **args)
{
  PetscErrorCode ierr;
  PetscInt       dim = 2, steps=10;
  PetscInt       ppcell = 2;
  PetscBool      use_plex = PETSC_TRUE;
  PetscBool      plex_simplex = PETSC_FALSE;

  ierr = PetscInitialize(&argc,&args,(char*)0,help);if (ierr) return ierr;
  ierr = PetscOptionsGetInt(NULL,NULL,"-dim",&dim,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-ppcell",&ppcell,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-steps",&steps,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-use_plex",&use_plex,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-simplex",&plex_simplex,NULL);CHKERRQ(ierr);
  /* quad / hex */
  ierr = pic_insert_DMPLEX(use_plex,plex_simplex,steps,dim,ppcell);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}
